require 'audio/bgm'

require 'graphics/graphics'

require 'logic/chat'

require 'logic/cg'

local engine_scene = require 'engine/scene'

function love.load()
    local font=love.graphics.newFont("data/font/msyhbd.ttf", 25)
    love.graphics.setFont(font)
    --bgimg = love.graphics.newImage("graphics/bg.png")
    chat_dialog_img = love.graphics.newImage("data/img/chat_dialog.png")
    lynne_cg_init()
    --lynne_graphics_init()

    engine_scene.init()
end

local chat = 1

function love.draw()
    --lynne_graphics_draw()
    --love.graphics.draw(bgimg, 0, 0)

    lynne_cg_draw()
    logic_chat_draw()

    engine_scene.draw()
end

function love.mousepressed(x, y, button)
    engine_scene.mousepressed(x, y, button)
    if button == 'l' then
    end
end

function love.mousereleased(x, y, button)
    engine_scene.mousereleased(x, y, button)
    if button == 'l' then
    end
end


function love.keypressed(key)
    engine_scene.keypressed(key)

    if key == '4' then
        chat = chat % 3 + 1
        lynne_logic_chat_play(chat)
    elseif key == 'q' then
        lynne_cg_load("魔法阵1")
    elseif key == 'w' then
        lynne_cg_load("魔法阵2")
    elseif key == 'e' then
        lynne_cg_load()
    end
end

function love.keyreleased(key)
    engine_scene.keyreleased(key)
end

function love.focus(f) g_gameIsPaused = not f end

function love.update(dt)
    if g_gameIsPaused then return end

    if love.keyboard.isDown( "escape" ) then love.event.quit() end

    local speed = 100 * dt
    --lynne_graphics_update(dt)

    engine_scene.update(dt)
end

function love.quit()
end
