local function draw()
    love.graphics.setColor(255, 255, 0, 255)
    love.graphics.print("关于！", 200, 100)
    love.graphics.reset()
    love.graphics.print("这是一个游戏。", 200, 150)
    love.graphics.print("【开发者】", 200, 200)
    love.graphics.print("zapline", 200, 250)
    love.graphics.print("Neko", 200, 300)
    love.graphics.print("转角", 200, 350)
    love.graphics.print("【顾问】", 200, 400)
    love.graphics.print("愤怒的泡面", 200, 450)
    love.graphics.print("Press 'Enter' to return.", 100, 500)
end

local function keypressed(key)
    if key == 'return' then
        local s = require('engine/scene')
        s.retreat()
    end
end

local x = {}
x.draw = draw
x.keypressed = keypressed
return x