
function lynne_graphics_map_load_tiles()
    g_map_tile = {}
    for i=1,4 do
        g_map_tile[i] = love.graphics.newImage( "data/tiles/tile"..i..".png" )
    end

    g_tile_w = 32
    g_tile_h = 32
end

function lynne_graphics_map_load_maps()
    g_map={
        { 3, 3, 3, 3, 3}, 
        { 3, 1, 4, 4, 2},
        { 3, 1, 4, 4, 2},
        { 4, 1, 4, 1, 1},
        { 4, 1, 4, 1, 4},
        { 4, 1, 4, 1, 1},
        { 4, 1, 4, 4, 4}}

    g_map_w = #g_map[1]
    g_map_h = #g_map
    g_map_x = 0
    g_map_y = 0
    g_map_display_buffer = 1
    g_map_display_w = 5
    g_map_display_h = 7
end
    
function lynne_graphics_map_draw()
    local offset_x = g_map_x % g_tile_w
    local offset_y = g_map_y % g_tile_h
    local firstTile_x = math.floor(g_map_x / g_tile_w)
    local firstTile_y = math.floor(g_map_y / g_tile_h)

    for y=1, (g_map_display_h + g_map_display_buffer) do
        for x=1, (g_map_display_w + g_map_display_buffer) do
            if y+firstTile_y >= 1 and y+firstTile_y <= g_map_h
                and x+firstTile_x >= 1 and x+firstTile_x <= g_map_w
            then
                love.graphics.draw(
                    g_map_tile[g_map[y+firstTile_y][x+firstTile_x]], 
                    ((x-1)*g_tile_w) - offset_x - g_tile_w/2, 
                    ((y-1)*g_tile_h) - offset_y - g_tile_h/2)
            end
        end
    end
end

function lynne_graphics_map_update(speed)
    if love.keyboard.isDown( "up" ) then
        g_map_y = g_map_y - speed
    end
    if love.keyboard.isDown( "down" ) then
        g_map_y = g_map_y + speed
    end

    if love.keyboard.isDown( "left" ) then
        g_map_x = g_map_x - speed
    end
    if love.keyboard.isDown( "right" ) then
        g_map_x = g_map_x + speed
    end

    if g_map_x < 0 then
        g_map_x = 0
    end
    if g_map_y < 0 then
        g_map_y = 0
    end 

    if g_map_x > g_map_w * g_tile_w - g_map_display_w * g_tile_w - 1 then
        g_map_x = g_map_w * g_tile_w - g_map_display_w * g_tile_w - 1
    end
    if g_map_y > g_map_h * g_tile_h - g_map_display_h * g_tile_h - 1 then
        g_map_y = g_map_h * g_tile_h - g_map_display_h * g_tile_h - 1
    end
end
